/* 	
	Lolabot v0.1
	Author(s): Daniel Thorpe, Rev. Taylor R. Rainwater

	- Since Dan cannot document then I will. 
	
	- This is the official Discord bot for the GNU/Fruitsalad
	server. Her name is Lola, named after Pyro's kitty. 
	Currently she just manages roles but is open (source) to 
	changes and additions.

	- If you work on the code: comment changes, update the 
	changelog, and make sure the code runs before making a 
	pull. Bad code will be rejected, no matter what. 

	Vocabulary Used In Comments:
		- User: 
			the server side stdin/stdout person.
		- Caller: 
			the Discord side io person who calls a command.
	TODO: add config file for Lola.
*/

package main

import (
	"bytes"
	"encoding/json"
	"flag"
	"fmt"
	"github.com/bwmarrin/discordgo"
	"github.com/spf13/viper"
	"gopkg.in/gomail.v2"
	"io/ioutil"
	"log"
	"math/rand"
	"os"
	"os/signal"
	"strconv"
	"strings"
	"syscall"
	"time"
)

const (
	VERSION_MAJOR = 1
	VERSION_MINOR = 2
	VERSION_PATCH = 4
	)
// OOOOOOOoooooo magic numbers.
const (
	ctomChannleID = 390439454718033921
	)

// Fancy vars for saucy lads.
var (
	versionString    = fmt.Sprintf("%d.%d.%d", VERSION_MAJOR, VERSION_MINOR, VERSION_PATCH)
	// Flags.
	flagDiscordToken string
	flagEmailPassword string
	flagEmailAddress string
	flagOwnerEmail   string
	flagConfig	 string
	// Some terrible globals to piss everyone off.
	err              error
	loaded		 bool
	loadedRoles      []*discordgo.Role
	dsession, _      = discordgo.New()
	// Mmmmmmmmm, hard coded arrays.
	excludedRoles	 = []string{"@everyone", "Owner", "Botberries", "Melons", "Oranges", "Apples", "Botcoins v2",
					"Numpty", "Muted", "Betabots", "EmojiTranslatorBot", "Weebiest Weeb", "Lacia",
					"Apples (users)", "Oranges (unverified)", "Pineapples (admins)", "Pears (mods)"}
	lolaGreetings 	 = []string{"Meowdy, purrdner! :cowboy:", "Hello! :smiley_cat:", "Hola, mi amigo! :smiley_cat:"}
	bullyPhrases	 = []string{"fuck you", "asshat", "cocktart", "faggot", "nigger"}
	)

// init function, stores the token from stdin. 
func init() {
	// Handle the flags.
	flag.StringVar(&flagDiscordToken, "t", "", "Discord token")
	flag.StringVar(&flagEmailPassword, "p", "", "Email Password")
	flag.StringVar(&flagEmailAddress, "e", "", "Email Address")
	flag.StringVar(&flagOwnerEmail, "o", "", "Owner Email Address")
	flag.StringVar(&flagConfig, "c", "", "Config File Name")
	// Set config file formatting type and default file name.
	viper.SetConfigType("yaml")
	viper.AddConfigPath(".")
	if flagConfig != "" {
		log.Println("Using provided config file.")
		viper.SetConfigName(flagConfig)
	} else {
		log.Println("Using default config file.")
		viper.SetConfigName("lola")
	}
	loaded = false
}


/*
	main
		Main function.
	Args:
		None.
 */
func main() {
	// Parse the commandline input.
	flag.Parse()
	// Read in config file.
	err := viper.ReadInConfig()
	if err != nil {
		log.Fatal("Error reading in config: " + err.Error())
	}
	// Check and see if the config file was provided something.
	if viper.GetString("key") != "" {
		flagDiscordToken = viper.GetString("key")
		flagOwnerEmail = viper.GetString("owner")
		flagEmailAddress = viper.GetString("emailaddr")
		flagEmailPassword = viper.GetString("emailpassword")
	}

	// Error out if user does not supply a token. 
	if flagDiscordToken == "" {
		log.Println("No Discord token specified.")
		os.Exit(0)
	}
	// Create a Discord session object, with the error handler defined, the type of Bot, and the supplied API token.
	dsession, err = discordgo.New("Bot " + flagDiscordToken)
	// Print error message to stdout.
	if err != nil {
		log.Fatal("Error creating Discord session:", err)
	}
	// Create a handler for when a message is sent to a channel the bot is in. 
	dsession.AddHandler(messageCreate)
	// Opens the Discord session,store errors in err variable, sets Discord status. 
	err = dsession.Open()
	err = dsession.UpdateStatus(0, "with testing version.")
	// Checks to see if there was an error, logs if there was.
	if err != nil {
		log.Fatal("Error opening discord ws conn:", err)
	}
	// No problems, client connected and bot running.
	log.Printf("Ready received!\nLola is running on version %s\nCtrl-c to stop.", versionString)

	//TODO: Implement a role loading, managing, etc. function to run here.

	// Kill bot process if the process receives any type of kill signal, else do nothing.
	sc := make(chan os.Signal, 1)
	signal.Notify(sc, syscall.SIGINT, syscall.SIGTERM, os.Interrupt, os.Kill)
	<-sc
	// Close the Discord session upon kill signal received.
	err = dsession.Close()
}

/*
	stringInSlice
		Find if a string is in an array of strings/slice.
	Args:
		a, the string to be found.
		list, the array/list/slice of strings.
	TODO: rename args.
 */
func stringInSlice(a string, list []string) bool {
	for _, b := range list {
		if b == a {
			return true
		}
	}
	return false
}
/*
	caseInsensitiveContains
		Finds a substring without concern for case.
	Args:
		s, string you are looking in.
		substr, the substring you are looking for.
 */
func caseInsensitiveContains(s, substr string) bool {
	s, substr = strings.ToUpper(s), strings.ToUpper(substr)
	return strings.Contains(s, substr)
}
/*
	deleteAllowedRolesFile
		Deletes the allowedRoles.json file.
	Args:
		None.
	TODO: Add functionality to delete other config files.
 */
func deleteAllowedRolesFile() {
	// delete file
	err = os.Remove("./allowedRoles.json")
	if err != nil {
		log.Println("Error deleting file.")
	}

	log.Println("Deleted 'allowedRoles.json'.")
}
/*
	createRolesFile
		Creates and initializes the allowedRoles.json file.
	Args:
		s, Discord session object pointer.
		m, message object pointer.
		guildID, ID of the Discord's guild.
	TODO: Clean up args and simplify loops.
 */
func createRolesFile(s *discordgo.Session, m *discordgo.MessageCreate, guildID string) (loadedRoles discordgo.Roles){
	// Make some vars

	var roleIDInt int
	rolesFile, err := os.Create("allowedRoles.json")

	if err != nil {
		log.Fatal("Error creating allowedRoles.json.")
	}

	log.Println("Creating roles file.")
	// Load the guild variable.
	guild, err := s.State.Guild(guildID)
	if err != nil {
		log.Fatal("Cannot get channel ID to populate.")
	}
	for _, role := range guild.Roles {
		for i := 0; i < len(guild.Roles); i++ {
			if guild.Roles[i].Name == role.Name {
				roleIDInt = i
			}
		}
		if stringInSlice(role.Name, excludedRoles){
			continue
		}
		log.Printf("Role %s added!", role.Name)
		loadedRoles = append(loadedRoles, role)
		addRoleAllowedRoles(roleIDInt, role.Name, guild.Roles, s, m)
	}
	_ = rolesFile.Close()
	return
}
/*
	addRoleAllowedRoles
		Adds a role to the allowedRoles.json file.
	Args:
		roleIDInt, index value for the role to be added.
		roleName, role name.
		guildRoles, an array of the available roles on the server.
		loadedRoles, an array of available roles.
		s, Discord session object pointer.
		m, message object pointer.
 */
func addRoleAllowedRoles(roleIDInt int, roleName string, guildRoles discordgo.Roles,
	s *discordgo.Session, m *discordgo.MessageCreate) {
	data, _ := json.Marshal(guildRoles[roleIDInt])
	log.Println("Begin file write")
	err = ioutil.WriteFile("./allowedRoles.json", []byte("["), 0644)
	if err != nil {
	}
	for i := 0; i < len(loadedRoles); i++ {
		data, _ := json.Marshal(loadedRoles[i])
		f, err := os.OpenFile("./allowedRoles.json", os.O_APPEND|os.O_WRONLY, 0644)
		if err != nil {
			panic(err)
		}

		if _, err = f.WriteString(string(data[:]) + ","); err != nil {
			panic(err)
		}

	}
	f, err := os.OpenFile("./allowedRoles.json", os.O_APPEND|os.O_WRONLY, 0644)
	if err != nil {
		panic(err)
	}

	if _, err = f.WriteString(string(data[:]) + "]"); err != nil {
		panic(err)
	}
	_, _ = s.ChannelMessageSend(m.ChannelID, "Added " + roleName + " to allowed roles.")
}
/*
	countingChannelAutoReply
		Checks if message is a counting channel message. If so, it increments the number
		and replies.
	Args:
		s, Discord session object pointer.
		m, message object pointer.
	TODO: check last message Lola sent.
 */
func countingChannelAutoReply(s *discordgo.Session, m *discordgo.Message) {
	channelIDInt, err := strconv.Atoi(m.ChannelID)
	if err != nil {
		log.Println("Error converting channel ID to integer.")
	}
	if  channelIDInt == ctomChannleID {
		previousInt, err := strconv.Atoi(m.Content)
		if err != nil {
			log.Println("Error converting message content to integer.")
		}
		if (rand.Intn(time.Now().Second()) % (rand.Intn(4) + 1)) == (rand.Intn(time.Now().Second()) % (rand.Intn(5) + 1))  {
			previousInt++
			previousString := fmt.Sprintf("%d", previousInt)
			log.Println("Sending a numeric reply! OwO")
			_, _ = s.ChannelMessageSend(m.ChannelID, previousString)
		}
	}
	return
}
/*
	plsNoBully
		Checks and responds to potential bullying remarks.
	Args:
		s, Discord session object pointer.
		m, message object pointer.
 */
func plsNoBully(s *discordgo.Session, m *discordgo.Message) {
	for _, insult := range bullyPhrases {
		if caseInsensitiveContains(m.Content, insult){
			_,_ = s.ChannelMessageSend(m.ChannelID, "pls no bully")
		}
	}
	return
}
/*
	sendmail
		Sends email based on the flags provided.
	Args:
		subject, the subject of the email.
		message, the message body of the email.
		emailaddr, the email address you would like to send to.
 */
func sendmail(subject string, message string, emailaddr string){
	m := gomail.NewMessage()
	m.SetHeader("From", flagEmailAddress)
	m.SetHeader("To", emailaddr)
	m.SetHeader("Subject", subject)
	m.SetBody("text/plain", message)
	log.Printf("Sending email from %s!", flagEmailAddress)
	d := gomail.NewDialer("smtp.gmail.com", 587, flagEmailAddress, flagEmailPassword)
	if err := d.DialAndSend(m); err != nil {
		log.Println("Error sending mail: " + err.Error())
	}
}
/* 
	messageCreate
		This is the message handler for the bot.
		Does many things, of which you can find documented
		in the function itself.
	Args: 
		s, Discord session pointer.
		m, Discord message creator pointer.
	TODO: Need to break this up into smaller functions.
 */
func messageCreate(s *discordgo.Session, m *discordgo.MessageCreate) {
	// This is the bot trigger.
	const prefix = "Lola, please"
	// This is array of allowed roles.
	if _, err := os.Stat("./allowedRoles.json"); os.IsNotExist(err) {
		loadedRoles = createRolesFile(s, m, m.GuildID)
	}
	// Check to see if the json file is there for roles.
	if _, err := os.Stat("./allowedRoles.json"); err == nil && loaded == false {
		// Read the json file in.
		log.Println("Checking roles file!")
		readFile, err := ioutil.ReadFile("./allowedRoles.json")
		// Let the user know the role list is read.
		if err != nil {
			log.Println("Opening roles file", err.Error())
		}
		// Decode the file.
		decode := json.NewDecoder(bytes.NewReader(readFile))
		// Store error variable.
		err = decode.Decode(&loadedRoles)
		// Tell the user if the was an error.
		if err != nil {
			log.Println(err)
		}
		// Set loaded switch to true.
		loaded = true
	} else if err != nil{
		// Ain't no got dang allowed roles in the file.
		log.Println("No allowed roles defined, please add some roles to be added.")
	}
	// What the fuck does this do?
	if m.Author.ID == s.State.User.ID {
		return
	}
	// Check if counting channel.
	countingChannelAutoReply(s, m.Message)
	// Check if bullying.
	plsNoBully(s, m.Message)
	// TODO: All this shit below this, fucking move it. Get this freshman style shit out of here.
	if strings.HasPrefix(m.Content, prefix + " send issue") {
		issue := m.Content[23:len(m.Content)]
		log.Println("Sending new issue!")
		sendmail("New Issue!", issue, flagOwnerEmail)
		_, _ = s.ChannelMessageSend(m.ChannelID, "Sending issue to owner!")
	}

	if strings.HasPrefix(m.Content, prefix + " poop") {
		log.Println("Sending requested poop!")
		_, _ = s.ChannelMessageSend(m.ChannelID, ":poop:")
	}

	if strings.Contains(m.Content, "💩") {
		log.Println("Sending automatic poop!")
		_, _ = s.ChannelMessageSend(m.ChannelID, ":poop:")
	}

	if strings.EqualFold(m.Content, "ayy") {
		log.Println("ayylmao :joy: :ok_hand: :fire:")
		_, _ = s.ChannelMessageSend(m.ChannelID, "lmao")
	}
	// TODO: Move this shit to a goddamn function and get this ugly ass if outta here. Use a slice, you fuckstick.
	if caseInsensitiveContains(m.Content,"lola") &&
			(caseInsensitiveContains(m.Content,"hi") ||
			caseInsensitiveContains(m.Content,"hello") ||
			caseInsensitiveContains(m.Content,"howdy") ||
			caseInsensitiveContains(m.Content,"what's up") ||
			caseInsensitiveContains(m.Content,"meowdy")||
			caseInsensitiveContains(m.Content,"hey")) {
		log.Println("Saying hi back!")
		_, _ = s.ChannelMessageSend(m.ChannelID, lolaGreetings[rand.Intn(3)])
	}
	// Store the ID information.
	channelID, _ := s.State.Channel(m.ChannelID)
	guildID, _ := s.Guild(channelID.GuildID)
	guildAdmin := guildID.OwnerID
	// This prints out the available roles in the json file.
	// Note: this is manually populated.
	if strings.HasPrefix(m.Content, prefix + " list") {
		log.Println("Listing roles!")
		availableRoles := "Roles available on this server: \n"
		availableRoles = availableRoles + "```\n"
		for _, role := range loadedRoles{
			availableRoles = availableRoles + role.Name + "\n"
		}
		availableRoles = availableRoles + "```"
		_, _ = s.ChannelMessageSend(m.ChannelID, availableRoles)
	}
	// This checks to see if the command is trying to add a role to the json file
	// and if the caller is the admin. If so, add the role to the json file.
	if strings.HasPrefix(m.Content, prefix+" add") && (m.Author.ID == guildAdmin) {
		var roleIDInt int
		log.Println("Adding a role!")
		roleName := m.Content[17:len(m.Content)]
		// Check to see if the role already exists.
		for i := 0; i < len(loadedRoles); i++ {
			if loadedRoles[i].Name == roleName {
				_, _ = s.ChannelMessageSend(m.ChannelID, "Role " + roleName + " is already in the list, and has not been added.")
				return
			}
		}
		// Load the channel variable.
		channel, err := s.State.Channel(m.ChannelID)
		if err != nil {
		}
		// Load the guildRoles variable.
		guildRoles, err := s.GuildRoles(channel.GuildID)
		if err != nil {
		}
		// Default out the roleID variable.
		roleID := ""
		// Check if the role is on the server.
		for i := 0; i < len(guildRoles); i++ {
			if guildRoles[i].Name == roleName {
				roleID = guildRoles[i].ID
				roleIDInt = i
			}
		}
		// If the role does not exist on the server, then let the caller know.
		if roleID == "" {
			_, _ = s.ChannelMessageSend(m.ChannelID, roleName + " was not found on this server.")
		} else {
			// Role exists on the server, now time to store in json.
			addRoleAllowedRoles(roleIDInt, roleName, guildRoles, s, m)
		}
	}
	// Reinitialize the allowedRoles.json file.
	if strings.HasPrefix(m.Content, prefix + " reinit") && (m.Author.ID == guildAdmin) {
		deleteAllowedRolesFile()
		loadedRoles = createRolesFile(s, m, m.GuildID)
		log.Println("Finished Reinitializing!")
	}
	// Add role to caller. 
	if strings.HasPrefix(m.Content, prefix+" apply") {
		roleName := m.Content[19:len(m.Content)]
		channel, err := s.State.Channel(m.ChannelID)
		if err != nil {
			log.Println("Error getting channel object for apply command!")
		}
		roleID := ""
		for i := 0; i < len(loadedRoles); i++ {
			if loadedRoles[i].Name == roleName {
				roleID = loadedRoles[i].ID
			}
		}
		if roleID == "" {
			_, _ = s.ChannelMessageSend(m.ChannelID, roleName + " was not found on this server.")
		} else {
			_ = s.GuildMemberRoleAdd(channel.GuildID, m.Author.ID, roleID)
			_, _ = s.ChannelMessageSend(m.ChannelID, "Given user <@"+m.Author.ID+"> " + roleName)
		}
	}
	// Remove role from caller.
	if strings.HasPrefix(m.Content, prefix+" remove") {
		roleName := m.Content[20:len(m.Content)]
		channel, err := s.State.Channel(m.ChannelID)
		if err != nil {
		}
		roleID := ""
		for i := 0; i < len(loadedRoles); i++ {
			if loadedRoles[i].Name == roleName {
				roleID = loadedRoles[i].ID
			}
		}
		if roleID == "" {
			_, _ = s.ChannelMessageSend(m.ChannelID, roleName + " was not found on this server.")
		} else {
			_ = s.GuildMemberRoleRemove(channel.GuildID, m.Author.ID, roleID)
			_, _ = s.ChannelMessageSend(m.ChannelID, "Removed user <@" + m.Author.ID + "> from " + roleName)
		}
	}

}
