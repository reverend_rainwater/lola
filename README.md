# FruitSaladBot
Basic bot designed for the GNU/Fruitsalad Server.

To build the bot, please run `go get -u gitlab.com/reverend_rainwater/lola`

## Running Lola: 
- Ensure you have `lola.yml` setup in the same directory as the executable.
- Either run the executable to use the default config file, or specify by using `lola -c "[config file name without extension]"`
